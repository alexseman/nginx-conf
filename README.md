# Nginx Conf 

Quick start nginx server confs.  
Based on most best practices from [H5BP](https://github.com/h5bp/server-configs-nginx) with config files that can be included or commented out. 

## Install

Best way is to  [stow](https://www.gnu.org/software/stow/) them in the nginx config dir (ex. `/etc/nginx` on most Debian based distros).

1. Clone the repository:

		git clone https://alexseman@bitbucket.org/alexseman/nginx-conf.git /etc/nginx/nginx-conf

2. Stow the dirs you want

		cd /etc/nginx/nginx-conf; stow config; stow sites
 
## Authors

* [**Alex Seman**](https://alexseman.com)
